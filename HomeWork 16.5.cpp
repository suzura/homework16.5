﻿
#include <iostream>
#include <time.h>

int main()
{
    
    const int size = 10;
    int array[size][size];
    int sum = 0;

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int  indeks = buf.tm_mday % size;

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                array[i][j] = i + j;
                
            }
        }

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                std::cout << array[i][j]<< " ";
                if (i == indeks) sum += array[i][j];
            }
            std::cout << '\n';
           
            
        }
        std::cout << '\n';
        std::cout << "Summa elementow stroki " << indeks << " = " << sum << '\n';

    return 0;
}

